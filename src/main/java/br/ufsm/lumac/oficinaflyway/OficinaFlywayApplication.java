package br.ufsm.lumac.oficinaflyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OficinaFlywayApplication {

    public static void main(String[] args) {
        SpringApplication.run(OficinaFlywayApplication.class, args);
    }

}
