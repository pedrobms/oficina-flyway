ALTER TABLE pessoa ADD COLUMN equipe VARCHAR(255);

UPDATE pessoa
SET equipe = t.equipenome
FROM (
         SELECT p.id AS pessoa_id,
                CASE
                    WHEN t.projeto_id = 1 THEN 'Equipe A'
                    WHEN t.projeto_id = 2 THEN 'Equipe B'
                    ELSE 'Outra Equipe'
                    END AS equipenome
         FROM tarefa t
                  JOIN pessoa p ON t.pessoa_id = p.id
     ) t
WHERE pessoa.id = t.pessoa_id;

