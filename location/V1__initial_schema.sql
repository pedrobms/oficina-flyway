CREATE TABLE projeto (
     id SERIAL PRIMARY KEY,
     nome VARCHAR(255) NOT NULL
);

CREATE TABLE pessoa (
    id SERIAL PRIMARY KEY,
    nome VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL
);

CREATE TABLE tarefa (
        id SERIAL PRIMARY KEY,
        descricao TEXT NOT NULL,
        projeto_id INTEGER REFERENCES projeto(id),
        pessoa_id INTEGER REFERENCES pessoa(id)
);

