INSERT INTO projeto (nome) VALUES
                              ('Desenvolvimento de Website E-Commerce'),
                              ('Implementação de Blog Corporativo');

INSERT INTO pessoa (nome, email) VALUES
                                     ('Felipe', 'felipe.m@example.com'),
                                     ('Pedro', 'pedro.b@example.com');

INSERT INTO tarefa (descricao, projeto_id, pessoa_id) VALUES
                                     ('Criar página de produtos do e-commerce', 1, 1),
                                     ('Implementar sistema de pagamento', 1, 1),
                                     ('Criar interface de administração do blog', 2, 2),
                                     ('Configurar servidor de hospedagem', 2, 2);
