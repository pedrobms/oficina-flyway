CREATE TABLE equipe (
        id SERIAL PRIMARY KEY,
        nome VARCHAR(255) NOT NULL,
        projeto_id INTEGER REFERENCES projeto(id)
);

INSERT INTO equipe (nome, projeto_id) SELECT DISTINCT equipe, p.id
                   FROM pessoa
                       JOIN tarefa t on pessoa.id = t.pessoa_id
                       JOIN projeto p on t.projeto_id = p.id;

ALTER TABLE pessoa ADD COLUMN equipe_id INTEGER REFERENCES equipe(id);
UPDATE pessoa SET equipe_id = e.id
FROM equipe e
WHERE pessoa.equipe = e.nome;

ALTER TABLE pessoa DROP COLUMN equipe;

ALTER TABLE tarefa ADD COLUMN equipe_id INTEGER REFERENCES equipe(id);
UPDATE tarefa
SET equipe_id = e.id
FROM equipe e JOIN projeto p on e.projeto_id = p.id JOIN tarefa t on p.id = t.projeto_id
WHERE tarefa.id = t.id;

ALTER TABLE tarefa DROP COLUMN projeto_id;
