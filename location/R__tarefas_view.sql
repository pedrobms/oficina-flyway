CREATE OR REPLACE VIEW tarefas_count AS
SELECT p.nome, COUNT(*) as qtd_tarefa
FROM tarefa t JOIN pessoa p ON t.pessoa_id = p.id
GROUP BY p.id;

-- After V4 change view

-- CREATE OR REPLACE VIEW tarefas_count AS
-- SELECT e.nome, COUNT(*) as qtd_tarefa
-- FROM tarefa t JOIN equipe e ON e.id = t.equipe_id
-- GROUP BY e.nome;
