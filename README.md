## Oficina Flyway

### Objetivo

O objetivo desta oficina é apresentar o Flyway, uma ferramenta de migração de banco de dados que permite versionar o banco de dados e controlar as alterações realizadas nele.

### Criar um banco de dados no Docker

Para criar um banco de dados no Docker, execute o seguinte comando:

```bash
docker run -d --name oficina-flyway-db -p 5432:5432 -e POSTGRES_PASSWORD=postgres postgres
```

### Criar os containers do banco de dados e do flyway no Docker

Rodar:

```bash
docker-compose up -d
```

### Rodar as migrations

Executa sempre a proxima migration:

```bash
docker-compose run --rm flyway migrate -n
```

Escolhe a versao a ser rodada :

docker-compose run --rm flyway migrate -n -target=<target_version>

Para ver info:

```bash
docker-compose run --rm flyway info
```
